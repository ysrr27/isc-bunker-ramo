--drop database prueba;
create database prueba;
use prueba;

--Creacion de Tablas  para el nombre de los campos no se puede usar - y lo cambie por _
DROP TABLE IF EXISTS employee;
CREATE TABLE employee (
  employee_name varchar(50) NOT NULL,
  street varchar(50) NOT NULL,
  city varchar(50) NOT NULL,
  PRIMARY KEY (employee_name)
);

DROP TABLE IF EXISTS company;
CREATE TABLE company (
  company_name varchar(50) NOT NULL,
  city varchar(50) NOT NULL,
  PRIMARY KEY (company_name)
);

DROP TABLE IF EXISTS works;
CREATE TABLE works (
  employee_name varchar(50) NOT NULL,
  company_name varchar(50) NOT NULL,
  salary int(8) NOT NULL,
  KEY fk_emp_work (employee_name),
  KEY fk_work_comp (company_name),  
  CONSTRAINT fk_work_comp FOREIGN KEY (company_name) REFERENCES company (company_name),
  CONSTRAINT fk_emp_work FOREIGN KEY (employee_name) REFERENCES employee (employee_name)
);

DROP TABLE IF EXISTS manages;
CREATE TABLE manages (
  employee_name varchar(50) NOT NULL,
  manager_name varchar(50) NOT NULL,
  KEY fk_emp_mang (employee_name),
  KEY fk_mang_emp (manager_name),    
  CONSTRAINT fk_mang_emp FOREIGN KEY (manager_name) REFERENCES employee (employee_name),
  CONSTRAINT fk_emp_mang FOREIGN KEY (employee_name) REFERENCES employee (employee_name)
);


--Data de prueba
insert into employee values ('peter','3784 st','New York');
insert into employee values ('Marcos','3900 st','New York');
insert into employee values ('Pedro','7708 st','Miami');
insert into employee values ('Luis','5643 st','Las vegas');
insert into employee values ('Carlos','5643 st','Las vegas');


insert into company values ('First Bank Corporation','New York');
insert into company values ('Medium Bank Corporation','Meryland');
insert into company values ('Small Bank Corporation','Miami');
insert into company values ('Large Bank Corporation','Miami');

insert into works values ('peter','Small Bank Corporation', 20000);
insert into works values ('Marcos','First Bank Corporation', 25000);
insert into works values ('Pedro','Small Bank Corporation', 15000);
insert into works values ('Luis','First Bank Corporation', 10000);
insert into works values ('Carlos','First Bank Corporation', 60000);

insert into manages values ('Carlos','Marcos');
insert into manages values ('Carlos','Luis');
insert into manages values ('peter','Pedro');


--querys
--a)
select employee_name, street, city from employee join works using (employee_name) where company_name = 'First Bank Corporation' and salary > 50000;
--b)
select employee_name from employee join works using (employee_name) join company using (company_name) where employee.city = company.city;
--c)
select a.employee_name
from manages a join employee b on (a.employee_name=b.employee_name) 
               join employee c on (a.manager_name=c.employee_name)
where b.street = c.street
and b.city = c.city;

--d) aqui no entendi que quiere decir con "asumir que todas las personas trabajan para exactamente una empresa" 
--esto tambien se puede resolver con un sub-query
select employee_name from works where company_name!='First Bank Corporation';
select employee_name from works where company_name not in (select company_name from works where company_name='First Bank Corporation' group by company_name);

--e) aqui no entendi que quiere decir con "asumir que todas las personas trabajan para, como mucho, una empresa"
select employee_name from works
where salary > (select max(salary) from works where company_name='Small Bank Corporation');

--f) qui no entendi "asumir que las empresas pueden estar ubicadas en varias ciudades" supongo que en este caso se refiere al uso del operador in en vez de =
select company_name from company
where city in (select city from company where company_name='Small Bank Corporation');








