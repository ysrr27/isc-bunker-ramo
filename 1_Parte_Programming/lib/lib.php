<?php

class Convert{
	public function display_number($string){
		if(gettype($string) == "string"){
			echo $this->convert_to_int($string);
		}else{
			echo $this->convert_to_string($string);
		}
	}
	
	public function convert_to_int($string){
        switch ($string) {
            case ("a"):
                $result = "0001";
                break;
            case ("b"):
                $result = "0002";
                break;
            case ("c"):
                $result = "0003";
                break;
            case ("d"):
                $result = "0004";
                break;
            case ("e"):
                $result = "0005";
                break;
            case ("f"):
                $result = "0006";
                break;
            case ("g"):
                $result = "0007";
                break;
            case ("h"):
                $result = "0008";
                break;
            case ("i"):
                $result = "0009";
                break;
            case ("j"):
                $result = "0010";
                break;
            case ("k"):
                $result = "0011";
                break;
            case ("l"):
                $result = "0012";
                break;
            case ("m"):
                $result = "0013";
                break;
            case ("n"):
                $result = "0014";
                break;
            case ("ñ"):
                $result = "0015";
                break;
            case ("o"):
                $result = "0016";
                break;
            case ("p"):
                $result = "0017";
                break;
            case ("q"):
                $result = "0018";
                break;
            case ("r"):
                $result = "0019";
                break;
            case ("s"):
                $result = "0020";
                break;
            case ("t"):
                $result = "0021";
                break;
            case ("u"):
                $result = "0022";
                break;
            case ("v"):
                $result = "0023";
                break;
            case ("w"):
                $result = "0024";
                break;
            case ("x"):
                $result = "0025";
                break;
            case ("y"):
                $result = "0026";
                break;
            case ("z"):
                $result = "0027";
                break;
            default:
                $result = "usted ingreso un caracter invalido";
        }
        return $result;
	}
	
	public function convert_to_string($int){
        switch ($int) {
            case (1):
                $result = "0001";
                break;
            case (2):
                $result = "0002";
                break;
            case (3):
                $result = "0003";
                break;
            case (4):
                $result = "0004";
                break;
            case (5):
                $result = "0005";
                break;
            case (6):
                $result = "0006";
                break;
            case (7):
                $result = "0007";
                break;
            case (8):
                $result = "0008";
                break;
            case (9):
                $result = "0009";
                break;
            case (10):
                $result = "0010";
                break;
            case (11):
                $result = "0011";
                break;
            case (12):
                $result = "0012";
                break;
            case (13):
                $result = "0013";
                break;
            case (14):
                $result = "0014";
                break;
            case (15):
                $result = "0015";
                break;
            case (16):
                $result = "0016";
                break;
            case (17):
                $result = "0017";
                break;
            case (18):
                $result = "0018";
                break;
            case (19):
                $result = "0019";
                break;
            case (20):
                $result = "0020";
                break;
            case (21):
                $result = "0021";
                break;
            case (22):
                $result = "0022";
                break;
            case (23):
                $result = "0023";
                break;
            case (24):
                $result = "0024";
                break;
            case (25):
                $result = "0025";
                break;
            case (26):
                $result = "0026";
                break;
            case (27):
                $result = "0027";
                break;
            default:
                $result = "usted ingreso un caracter invalido";
        }
        return $result;
	}
}

?>
